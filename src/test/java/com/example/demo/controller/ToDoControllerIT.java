package com.example.demo.controller;

import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.exception.ToDoNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.util.Arrays;
import java.util.Optional;

import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.model.ToDoEntity;
import com.example.demo.service.ToDoService;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
class ToDoControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ToDoService toDoService;

    @Test
    void whenGetAll_thenReturnValidResponse() throws Exception {
        String testText = "My to do text";
        Long testId = 1l;
        when(toDoService.getAll()).thenReturn(
                Arrays.asList(
                        ToDoEntityToResponseMapper.map(new ToDoEntity(testId, testText))
                )
        );

        this.mockMvc
                .perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].text").value(testText))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(testId))
                .andExpect(jsonPath("$[0].completedAt").doesNotExist());
    }

    // Все тесты ниже, не проходят, хотя в Postman все статусы как я указал ниже
    @Test
    void whenCompleteWrongId_thenThrowExceptionStatus404() throws Exception {
        long id = 0L;

        when(toDoService.completeToDo(anyLong()))
                .thenThrow(new ToDoNotFoundException(id));

        this.mockMvc
                .perform(put("/todos/" + id + "/complete"))
                .andExpect(status().isOk())
                .andExpect(status().reason("Can not find todo with such id " + id));
    }

    @Test
    void whenGetWithWrongId_thenThrowExceptionStatus404() throws Exception {
        long id = 0L;

        when(toDoService.getOne(anyLong()))
                .thenThrow(new ToDoNotFoundException(0L));

        this.mockMvc
                .perform(get("/todos/" + id))
                .andExpect(status().isOk())
                .andExpect(status().reason("Can not find todo with such id " + id));
    }

    @Test
    void whenIdExist_thenReturnToDoWithItsId() throws Exception {

        long id = 0L;
        this.mockMvc
                .perform(delete("/todos/" + id))
                .andExpect(status().is5xxServerError())
                .andExpect(status().reason("No class com.example.demo.model.ToDoEntity entity with id " + id + " exists!"));
    }
}